package main

import (
	"flag"
	"log"
	"os"

	"gitlab.com/go-lfx/gen-obj-pool/codegen"
)

var (
	typeName        = flag.String("type", "", "Type for which object pool is to be generated")
	poolTypeName    = flag.String("pool-type", "", "Type of the object pool")
	poolPackageName = flag.String("pool-package", "", "Generated package name")
	outFileName     = flag.String("file", "", "Path of the output file")
	withRelease     = flag.Bool("with-release", false, "Include Release() receiver in the pool")
	threadsafe      = flag.Bool("threadsafe", false, "Make the pool threadsafe")
)

func main() {
	parseFlags()

	gen, err := codegen.NewGenerator()
	if err != nil {
		log.Println("Failed to load code generator. err=", err)
		os.Exit(1)
	}

	outFile, err := os.Create(*outFileName)
	if err != nil {
		log.Println("Failed to open file", outFileName, "err=", err)
		os.Exit(1)
	}
	defer outFile.Close()

	genParam := codegen.GenParam{
		ObjectType:      *typeName,
		PoolTypeName:    *poolTypeName,
		PoolPackageName: *poolPackageName,
		IsThreadsafe:    *threadsafe,
		WithRelease:     *withRelease,
	}

	if err := gen.Generate(outFile, genParam); err != nil {
		log.Println("Failed to generate code. err=", err)
		os.Exit(1)
	}
}

func parseFlags() {
	flag.Parse()
	argErr := false

	if outFileName == nil || len(*outFileName) == 0 {
		log.Println("Missing argument - file")
		argErr = true
	}

	if poolPackageName == nil || len(*poolPackageName) == 0 {
		log.Println("Missing argument - pool-package")
		argErr = true
	}

	if poolTypeName == nil || len(*poolTypeName) == 0 {
		log.Println("Missing argument - pool-type")
		argErr = true
	}

	if typeName == nil || len(*typeName) == 0 {
		log.Println("Missing argument - type")
		argErr = true
	}

	if argErr {
		os.Exit(1)
	}
}
