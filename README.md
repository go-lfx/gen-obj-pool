# Introduction
`gen-obj-pool` is used to generate an object pool for a specific type.  This is helpful in improving the performance of applications that create several objects of same type.

It improves performance by allocating a block of objects at a time.

# Usage

## Installation

```sh
go install gitlab.com/go-lfx/gen-obj-pool/cmd/gen-obj-pool
```

## Use with go:generate
```go
//go:generate gen-obj-pool -file=./logentry.go -pool-package=logger -pool-type=LogEntryPool -type=LogEntry
type LogEntry struct {
	// ...
}
```

This will generate a file `./logentry.go`, which will have the following structure:

```go
package logger

type LogEntryPool struct {
    // ...
}

func (op *LogEntryPool) Allocate() *LogEntry {
    // ...
}
```

### With Release
```go
//go:generate gen-obj-pool -file=./logentry.go -pool-package=logger -pool-type=LogEntryPool -type=LogEntry -with-release
type LogEntry struct {
	// ...
}
```

With `-with-release` flag, it will add a `Release()` receiver to the generated code that allows reuse of allocated objects:

```go
package logger

type LogEntryPool struct {
    // ...
}

func (op *LogEntryPool) Release(*LogEntry) {
    // ...
}
```

### Threadsafe
```go
//go:generate gen-obj-pool -file=./logentry.go -pool-package=logger -pool-type=LogEntryPool -type=LogEntry -threadsafe
type LogEntry struct {
	// ...
}
```

The `-threadsafe` flag will make the code generated safe for use from multiple go-routines.  This will have a performance impact on the allocation due to lock contention.
