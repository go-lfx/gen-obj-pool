package codegen

import (
	"bytes"
	"io/ioutil"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGenerate(t *testing.T) {
	gen, err := NewGenerator()
	require.NoError(t, err)
	require.NotNil(t, gen)

	var outBuf bytes.Buffer

	genParam := GenParam{
		ObjectType:      "TestObject",
		PoolTypeName:    "TestObjectPool",
		PoolPackageName: "testpool",
		IsThreadsafe:    false,
		WithRelease:     false,
	}
	err = gen.Generate(&outBuf, genParam)
	require.NoError(t, err)

	expectedData, err := ioutil.ReadFile("./testpool/testpool.go")
	require.NoError(t, err)

	require.Equal(t, normalizeTestGenBuf(expectedData), normalizeTestGenBuf(outBuf.Bytes()))
}

func TestGenerateWithRelease(t *testing.T) {
	gen, err := NewGenerator()
	require.NoError(t, err)
	require.NotNil(t, gen)

	var outBuf bytes.Buffer

	genParam := GenParam{
		ObjectType:      "TestObject",
		PoolTypeName:    "TestObjectPoolWithRelease",
		PoolPackageName: "testpool",
		IsThreadsafe:    false,
		WithRelease:     true,
	}
	err = gen.Generate(&outBuf, genParam)
	require.NoError(t, err)

	expectedData, err := ioutil.ReadFile("./testpool/testpool_with_release.go")
	require.NoError(t, err)

	require.Equal(t, normalizeTestGenBuf(expectedData), normalizeTestGenBuf(outBuf.Bytes()))
}

func TestGenerateWithReleaseThreadsafe(t *testing.T) {
	gen, err := NewGenerator()
	require.NoError(t, err)
	require.NotNil(t, gen)

	var outBuf bytes.Buffer

	genParam := GenParam{
		ObjectType:      "TestObject",
		PoolTypeName:    "TestObjectPoolWithReleaseThreadsafe",
		PoolPackageName: "testpool",
		IsThreadsafe:    true,
		WithRelease:     true,
	}
	err = gen.Generate(&outBuf, genParam)
	require.NoError(t, err)

	expectedData, err := ioutil.ReadFile("./testpool/testpool_with_release_threadsafe.go")
	require.NoError(t, err)

	require.Equal(t, normalizeTestGenBuf(expectedData), normalizeTestGenBuf(outBuf.Bytes()))
}

func normalizeTestGenBuf(inBuf []byte) string {
	outStr := string(inBuf)
	replacer := strings.NewReplacer(" ", "", "\t", "", "\n", "", "\r", "")

	outStr = replacer.Replace(outStr)
	return outStr
}
