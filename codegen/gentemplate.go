package codegen

// genTemplate is the template used for the code generation.
const genTemplate = `
package {{.PoolPackageName}}
{{ if .IsThreadsafe -}}import (
	"sync"
){{- end }}

// {{.PoolTypeName}}BuilderType builds a {{.PoolTypeName}} object.
type {{.PoolTypeName}}BuilderType struct {
	blockSize uint
}

func {{.PoolTypeName}}Builder() *{{.PoolTypeName}}BuilderType {
	return &{{.PoolTypeName}}BuilderType {
		blockSize: 16 * 1024,
	}
}

func (b *{{.PoolTypeName}}BuilderType) WithBlockSize(blockSize uint) *{{.PoolTypeName}}BuilderType {
	b.blockSize = blockSize
	return b
}

func (b *{{.PoolTypeName}}BuilderType) Build() *{{.PoolTypeName}} {
	return new{{.PoolTypeName}}(b.blockSize)
}

func new{{.PoolTypeName}}(blockSize uint) *{{.PoolTypeName}} {
	pool := &{{.PoolTypeName}}{
		blockSize: blockSize,
	}

	pool.createPool()

	return pool
}

// {{.PoolTypeName}} implements an object pool for {{.ObjectType}}.
type {{.PoolTypeName}} struct {
	freeObjects []*{{.ObjectType}}
	blockSize   uint
	nextAllocIndex uint
	{{ if .IsThreadsafe -}}
	mutex sync.Mutex
	{{- end }}
}

func (op *{{.PoolTypeName}}) Allocate() *{{.ObjectType}} {
	{{ if .IsThreadsafe }}
	op.mutex.Lock()
	defer op.mutex.Unlock()

	{{ end -}}

	if op.isPoolEmpty() {
		op.createPool()
	}

	return op.allocFromPool()
}

{{ if .WithRelease -}}
func (op *{{.PoolTypeName}}) Release(obj *{{.ObjectType}}) {
	{{ if .IsThreadsafe -}}
	op.mutex.Lock()
	defer op.mutex.Unlock()
	{{- end }}

	op.restoreToPool(obj)
}
{{- end }}

func (op *{{.PoolTypeName}}) isPoolEmpty() bool {
	return op.nextAllocIndex == op.blockSize
}

func (op *{{.PoolTypeName}}) isPoolFull() bool {
	return op.nextAllocIndex == 0
}

func (op *{{.PoolTypeName}}) allocFromPool() *{{.ObjectType}} {
	allocated := op.freeObjects[op.nextAllocIndex]
	op.freeObjects[op.nextAllocIndex] = nil
	op.nextAllocIndex += 1

	return allocated
}

func (op *{{.PoolTypeName}}) createPool() {
	newObjs := make([]{{.ObjectType}}, op.blockSize, op.blockSize)
	op.freeObjects = make([]*{{.ObjectType}}, op.blockSize, op.blockSize)

	for i := 0; i < len(newObjs); i++ {
		op.freeObjects[i] = &newObjs[i]
	}

	op.nextAllocIndex = 0
}

{{ if .WithRelease -}}
// restoreToPool restores the object to the pool if there is space in the pool.
// If the pool is full, the object is discarded.
func (op *{{.PoolTypeName}}) restoreToPool(obj *{{.ObjectType}}) bool {
	if op.isPoolFull() {
		return false
	}

	op.nextAllocIndex--
	op.freeObjects[op.nextAllocIndex] = obj
	return true
}
{{- end }}
`
