package codegen

import (
	"io"
	"text/template"
)

// NewGenerator creates Generator object with the code generation
// templates loaded.
func NewGenerator() (*Generator, error) {
	genTemplate, err := template.New("codeGen").Parse(genTemplate)
	if err != nil {
		return nil, err
	}

	gen := &Generator{
		genTemplate: genTemplate,
	}

	return gen, nil
}

// Generator generates the object pool code.
type Generator struct {
	genTemplate *template.Template
}

// GenParam holds the parameters that control the code generation.
type GenParam struct {
	ObjectType      string
	PoolTypeName    string
	PoolPackageName string

	// If true, the generated type is made threadsafe by adding
	// a mutex to serialize access.
	IsThreadsafe bool

	// If true, the generate code will have a Release receiver
	// to release the allocated object back to the pool
	WithRelease bool
}

// Generate expands the code template based on the given parameters and
// writes the output to `wr`.
func (g *Generator) Generate(wr io.Writer, param GenParam) error {
	return g.genTemplate.Execute(wr, param)
}
