package testpool

import (
	"testing"

	"github.com/stretchr/testify/require"
)

type allocInterface interface {
	Allocate() *TestObject
}

type releaseInterface interface {
	Release(*TestObject)
}

// TestGenerate - Code generated without any optional flags should have
// `Allocate()` receiver implemented, but not `Release()`.
func TestGenerate(t *testing.T) {
	var iPool interface{}
	iPool = TestObjectPoolBuilder().WithBlockSize(4).Build()

	pool, ok := iPool.(allocInterface)
	require.True(t, ok, "Pool should implement 'Allocate()'")

	_, ok = iPool.(releaseInterface)
	require.False(t, ok, "Pool should not implement 'Release()'")

	for i := 0; i < 100; i++ {
		testObj := pool.Allocate()
		require.NotNil(t, testObj, "Allocation failed at %d", i)
	}
}

// TestGenerateWithRelease - Code generated with `-with-release` flag should have
// `Allocate()` and `Release()` receiver implemented.
func TestGenerateWithRelease(t *testing.T) {
	var iPool interface{}
	iPool = TestObjectPoolWithReleaseBuilder().WithBlockSize(4).Build()

	_, ok := iPool.(allocInterface)
	require.True(t, ok, "Pool should implement 'Allocate()'")

	_, ok = iPool.(releaseInterface)
	require.True(t, ok, "Pool should implement 'Release()'")

	pool := iPool.(*TestObjectPoolWithRelease)

	for i := 0; i < 100; i++ {
		testObj := pool.Allocate()
		require.NotNil(t, testObj, "Allocation failed at %d", i)
		pool.Release(testObj)
	}
}

// TestGenerateWithReleaseThreadsafe - Code generated with `-with-release -threadsafe` \
// flag should have `Allocate()` and `Release()` implemented.
func TestGenerateWithReleaseThreadsafe(t *testing.T) {
	var iPool interface{}
	iPool = TestObjectPoolWithReleaseThreadsafeBuilder().WithBlockSize(4).Build()

	_, ok := iPool.(allocInterface)
	require.True(t, ok, "Pool should implement 'Allocate()'")

	_, ok = iPool.(releaseInterface)
	require.True(t, ok, "Pool should implement 'Release()'")

	pool := iPool.(*TestObjectPoolWithReleaseThreadsafe)

	for i := 0; i < 100; i++ {
		testObj := pool.Allocate()
		require.NotNil(t, testObj, "Allocation failed at %d", i)
		pool.Release(testObj)
	}
}
