
package testpool


// TestObjectPoolBuilderType builds a TestObjectPool object.
type TestObjectPoolBuilderType struct {
	blockSize uint
}

func TestObjectPoolBuilder() *TestObjectPoolBuilderType {
	return &TestObjectPoolBuilderType {
		blockSize: 16 * 1024,
	}
}

func (b *TestObjectPoolBuilderType) WithBlockSize(blockSize uint) *TestObjectPoolBuilderType {
	b.blockSize = blockSize
	return b
}

func (b *TestObjectPoolBuilderType) Build() *TestObjectPool {
	return newTestObjectPool(b.blockSize)
}

func newTestObjectPool(blockSize uint) *TestObjectPool {
	pool := &TestObjectPool{
		blockSize: blockSize,
	}

	pool.createPool()

	return pool
}

// TestObjectPool implements an object pool for TestObject.
type TestObjectPool struct {
	freeObjects []*TestObject
	blockSize   uint
	nextAllocIndex uint
	
}

func (op *TestObjectPool) Allocate() *TestObject {
	if op.isPoolEmpty() {
		op.createPool()
	}

	return op.allocFromPool()
}



func (op *TestObjectPool) isPoolEmpty() bool {
	return op.nextAllocIndex == op.blockSize
}

func (op *TestObjectPool) isPoolFull() bool {
	return op.nextAllocIndex == 0
}

func (op *TestObjectPool) allocFromPool() *TestObject {
	allocated := op.freeObjects[op.nextAllocIndex]
	op.freeObjects[op.nextAllocIndex] = nil
	op.nextAllocIndex += 1

	return allocated
}

func (op *TestObjectPool) createPool() {
	newObjs := make([]TestObject, op.blockSize, op.blockSize)
	op.freeObjects = make([]*TestObject, op.blockSize, op.blockSize)

	for i := 0; i < len(newObjs); i++ {
		op.freeObjects[i] = &newObjs[i]
	}

	op.nextAllocIndex = 0
}


