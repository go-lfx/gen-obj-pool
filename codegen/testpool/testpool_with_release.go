
package testpool


// TestObjectPoolWithReleaseBuilderType builds a TestObjectPoolWithRelease object.
type TestObjectPoolWithReleaseBuilderType struct {
	blockSize uint
}

func TestObjectPoolWithReleaseBuilder() *TestObjectPoolWithReleaseBuilderType {
	return &TestObjectPoolWithReleaseBuilderType {
		blockSize: 16 * 1024,
	}
}

func (b *TestObjectPoolWithReleaseBuilderType) WithBlockSize(blockSize uint) *TestObjectPoolWithReleaseBuilderType {
	b.blockSize = blockSize
	return b
}

func (b *TestObjectPoolWithReleaseBuilderType) Build() *TestObjectPoolWithRelease {
	return newTestObjectPoolWithRelease(b.blockSize)
}

func newTestObjectPoolWithRelease(blockSize uint) *TestObjectPoolWithRelease {
	pool := &TestObjectPoolWithRelease{
		blockSize: blockSize,
	}

	pool.createPool()

	return pool
}

// TestObjectPoolWithRelease implements an object pool for TestObject.
type TestObjectPoolWithRelease struct {
	freeObjects []*TestObject
	blockSize   uint
	nextAllocIndex uint
	
}

func (op *TestObjectPoolWithRelease) Allocate() *TestObject {
	if op.isPoolEmpty() {
		op.createPool()
	}

	return op.allocFromPool()
}

func (op *TestObjectPoolWithRelease) Release(obj *TestObject) {
	

	op.restoreToPool(obj)
}

func (op *TestObjectPoolWithRelease) isPoolEmpty() bool {
	return op.nextAllocIndex == op.blockSize
}

func (op *TestObjectPoolWithRelease) isPoolFull() bool {
	return op.nextAllocIndex == 0
}

func (op *TestObjectPoolWithRelease) allocFromPool() *TestObject {
	allocated := op.freeObjects[op.nextAllocIndex]
	op.freeObjects[op.nextAllocIndex] = nil
	op.nextAllocIndex += 1

	return allocated
}

func (op *TestObjectPoolWithRelease) createPool() {
	newObjs := make([]TestObject, op.blockSize, op.blockSize)
	op.freeObjects = make([]*TestObject, op.blockSize, op.blockSize)

	for i := 0; i < len(newObjs); i++ {
		op.freeObjects[i] = &newObjs[i]
	}

	op.nextAllocIndex = 0
}

// restoreToPool restores the object to the pool if there is space in the pool.
// If the pool is full, the object is discarded.
func (op *TestObjectPoolWithRelease) restoreToPool(obj *TestObject) bool {
	if op.isPoolFull() {
		return false
	}

	op.nextAllocIndex--
	op.freeObjects[op.nextAllocIndex] = obj
	return true
}
