package testpool

//go:generate gen-obj-pool -file=./testpool.go -pool-package=testpool -pool-type TestObjectPool -type=TestObject
//go:generate gen-obj-pool -file=./testpool_with_release.go -pool-package=testpool -pool-type TestObjectPoolWithRelease -type=TestObject -with-release
//go:generate gen-obj-pool -file=./testpool_with_release_threadsafe.go -pool-package=testpool -pool-type=TestObjectPoolWithReleaseThreadsafe -type=TestObject -with-release -threadsafe
type TestObject struct {
	Value int
}
