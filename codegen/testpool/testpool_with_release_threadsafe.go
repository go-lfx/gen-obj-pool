
package testpool
import (
	"sync"
)

// TestObjectPoolWithReleaseThreadsafeBuilderType builds a TestObjectPoolWithReleaseThreadsafe object.
type TestObjectPoolWithReleaseThreadsafeBuilderType struct {
	blockSize uint
}

func TestObjectPoolWithReleaseThreadsafeBuilder() *TestObjectPoolWithReleaseThreadsafeBuilderType {
	return &TestObjectPoolWithReleaseThreadsafeBuilderType {
		blockSize: 16 * 1024,
	}
}

func (b *TestObjectPoolWithReleaseThreadsafeBuilderType) WithBlockSize(blockSize uint) *TestObjectPoolWithReleaseThreadsafeBuilderType {
	b.blockSize = blockSize
	return b
}

func (b *TestObjectPoolWithReleaseThreadsafeBuilderType) Build() *TestObjectPoolWithReleaseThreadsafe {
	return newTestObjectPoolWithReleaseThreadsafe(b.blockSize)
}

func newTestObjectPoolWithReleaseThreadsafe(blockSize uint) *TestObjectPoolWithReleaseThreadsafe {
	pool := &TestObjectPoolWithReleaseThreadsafe{
		blockSize: blockSize,
	}

	pool.createPool()

	return pool
}

// TestObjectPoolWithReleaseThreadsafe implements an object pool for TestObject.
type TestObjectPoolWithReleaseThreadsafe struct {
	freeObjects []*TestObject
	blockSize   uint
	nextAllocIndex uint
	mutex sync.Mutex
}

func (op *TestObjectPoolWithReleaseThreadsafe) Allocate() *TestObject {
	
	op.mutex.Lock()
	defer op.mutex.Unlock()

	if op.isPoolEmpty() {
		op.createPool()
	}

	return op.allocFromPool()
}

func (op *TestObjectPoolWithReleaseThreadsafe) Release(obj *TestObject) {
	op.mutex.Lock()
	defer op.mutex.Unlock()

	op.restoreToPool(obj)
}

func (op *TestObjectPoolWithReleaseThreadsafe) isPoolEmpty() bool {
	return op.nextAllocIndex == op.blockSize
}

func (op *TestObjectPoolWithReleaseThreadsafe) isPoolFull() bool {
	return op.nextAllocIndex == 0
}

func (op *TestObjectPoolWithReleaseThreadsafe) allocFromPool() *TestObject {
	allocated := op.freeObjects[op.nextAllocIndex]
	op.freeObjects[op.nextAllocIndex] = nil
	op.nextAllocIndex += 1

	return allocated
}

func (op *TestObjectPoolWithReleaseThreadsafe) createPool() {
	newObjs := make([]TestObject, op.blockSize, op.blockSize)
	op.freeObjects = make([]*TestObject, op.blockSize, op.blockSize)

	for i := 0; i < len(newObjs); i++ {
		op.freeObjects[i] = &newObjs[i]
	}

	op.nextAllocIndex = 0
}

// restoreToPool restores the object to the pool if there is space in the pool.
// If the pool is full, the object is discarded.
func (op *TestObjectPoolWithReleaseThreadsafe) restoreToPool(obj *TestObject) bool {
	if op.isPoolFull() {
		return false
	}

	op.nextAllocIndex--
	op.freeObjects[op.nextAllocIndex] = obj
	return true
}
